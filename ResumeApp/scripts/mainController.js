(function () {
    'use strict';
 
    var resumeApp = angular.module('resumeApp');
    resumeApp.controller('mainController', ['$scope', function ($scope) {
       
        $scope.who = "Bhavika Tekwani";

        $scope.message = "I design and build web apps.";
        $scope.hireme = "I am available for projects. Hire me.";
        $scope.extra = "I'm interested in open source technologies, tech culture, music, feminism and diversity.";

    }]);
 
}());