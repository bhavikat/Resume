
var resumeApp = angular.module('resumeApp', ["ngRoute"]);

 resumeApp.config(function($routeProvider)
 {
        $routeProvider.when('/index',
            {
                templateUrl: "/index.html",
                controller : "mainController"
            })
            .when('/skills',
            {
                templateUrl:"/skills.html",
                controller: "skillsController"
            })
            .when('/contact',
            {
                templateUrl: "/contact.html",
                controller: "contactController"
            })
            .otherwise({redirectTo: '/'});
    });

